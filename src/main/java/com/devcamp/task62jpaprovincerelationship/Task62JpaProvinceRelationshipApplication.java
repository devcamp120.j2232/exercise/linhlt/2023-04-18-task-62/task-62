package com.devcamp.task62jpaprovincerelationship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task62JpaProvinceRelationshipApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task62JpaProvinceRelationshipApplication.class, args);
	}

}
