package com.devcamp.task62jpaprovincerelationship.services;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task62jpaprovincerelationship.model.District;
import com.devcamp.task62jpaprovincerelationship.model.Province;
import com.devcamp.task62jpaprovincerelationship.repository.IProvinceRepository;

@Service
public class ProvinceService {
    @Autowired
    IProvinceRepository provinceRepository;
    public ArrayList<Province> getAllProvinces(){
        ArrayList<Province> provinceList = new ArrayList<>();
        provinceRepository.findAll().forEach(provinceList:: add);
        return provinceList;
    }
    public Set<District> getDistrictsByProvinceId(int id){
        Province vProvince = provinceRepository.findById(id);
        if ( vProvince != null){
            return vProvince.getDistricts();
        }
        else return null;
    }

}
