package com.devcamp.task62jpaprovincerelationship.controllers;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task62jpaprovincerelationship.model.District;
import com.devcamp.task62jpaprovincerelationship.model.Province;
import com.devcamp.task62jpaprovincerelationship.services.ProvinceService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ProvinceController {
    @Autowired
    ProvinceService provinceService;
    
    //get all provinces lisst
    @GetMapping("/provinces")
    public ResponseEntity<List<Province>> getAllProvinces(){
        try {
            return new ResponseEntity<>(provinceService.getAllProvinces(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    //get district by province id
    @GetMapping("/districts")
    public ResponseEntity<Set<District>> getDistrictsByProvinceIdApi(@RequestParam(value = "provinceId") int id){
        try {
            Set<District> provinceDistricts = provinceService.getDistrictsByProvinceId(id);
            if (provinceDistricts != null ){
                return new ResponseEntity<>(provinceDistricts, HttpStatus.OK);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
