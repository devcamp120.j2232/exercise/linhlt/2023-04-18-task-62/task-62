package com.devcamp.task62jpaprovincerelationship.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task62jpaprovincerelationship.model.District;

public interface IDistrictRepository extends JpaRepository<District, Long>{
    District findById(int id);
}
